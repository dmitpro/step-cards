# Step project Cards



Leader of project: [Dmytro Protsenko](https://gitlab.com/dmitpro)

Co-leaders: [Sviatoslav Malyshevskyi](https://gitlab.com/sviatoslav.malyshevskyi) , [Oleh Horskyj](https://gitlab.com/ogorski)

Objectives for **Dmytro Protsenko**:<br>
AJAX-request, GET|POST requests, authorization function, 
card sorting, card deleting, Form class, Webpack settings, [gitlab pages](https://dmitpro.gitlab.io/step-cards/).

Objectives for **Oleh Horskyj**:<br> 
Cards design, Visit function, Modal class. 

Objectives for **Sviatoslav Malyshevskyi**: <br>
Drag&Drop function, Card editing, PUT request.


>Authorization for testing:<br>
>login: a@a.com<br>
>password: 12345

>password for deleting: 123
